const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./Routes/userRoutes.js');
const productRoutes = require('./Routes/productRoutes.js');

const app = express();

mongoose.set('strictQuery', true);

const port = 3005;

mongoose.connect(
  'mongodb+srv://admin:admin@batch245-dumaug.18ijatd.mongodb.net/capstone_PRODUCT_API_Dumaug?retryWrites=true&w=majority',
  {
    // Allows us to avoid any acurrent and future errors while connecting to mongodb.
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// check connection
let db = mongoose.connection;

//error catcher
db.on('error', console.error.bind(console, 'Connection Error!'));

//Confirmation of the connection
db.once('open', () => console.log('We are now connected to the cloud!'));

// NOTE: Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// routing
app.use('/user', userRoutes);
app.use('/product', productRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`));

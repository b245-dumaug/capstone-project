const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Name of the course is required!'],
  },
  description: {
    type: String,
    required: [true, 'Description of the course is required!'],
  },
  price: {
    type: Number,
    required: [true, 'Price of the course is required!'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: Date.now,
  },
  orders: [
    {
      productId: {
        type: String,
        required: true,
      },
      name: {
        type: String,
        required: true,
      },
      description: {
        type: String,
        required: [true, 'Description of the course is required!'],
      },
      price: {
        type: Number,
        required: [true, 'Price of the course is required!'],
      },
      createdOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: 'Order Created',
      },
    },
  ],
});

module.exports = mongoose.model('Product', productSchema);

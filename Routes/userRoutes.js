const express = require('express');
const router = express.Router();
const userController = require('../Controllers/userController.js');
const auth = require('../auth.js');

//  Register a new user
router.post('/register', userController.userRegistration);

// Authenticate user
router.post('/login', userController.userAuthentication);

// Get profile details
router.get('/details', auth.verify, userController.getProfile);

// Post orders
router.post('/orders/:productId', auth.verify, userController.checkoutOrders);

// Set user as admin
router.post('/setasadmin/:userId', auth.verify, userController.setAsAdmin);

// Get authenticated user orders
router.get('/orders', auth.verify, userController.getUserOrders);

// Get all orders (Admin only)
router.get('/orders/admin', userController.getOrders);

module.exports = router;

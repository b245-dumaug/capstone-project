const express = require('express');
const router = express.Router();
const productController = require('../Controllers/productController');
const auth = require('../auth.js');

// Route for creating a product
router.post('/', auth.verify, productController.addProduct);

// Route for retrieving all active products
router.get('/allactive', productController.allActiveProducts);

// Route for retriveing all products
router.get('/all', productController.allProducts);

//Route for retrieving specific product.
router.get('/:productId', productController.productDetails);

// Route for updating a product
router.put('/update/:productId', auth.verify, productController.updateProduct);

//Route for archiving product
router.put(
  '/archive/:productId',
  auth.verify,
  productController.archiveProduct
);

module.exports = router;

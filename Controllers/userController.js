const mongoose = require('mongoose');
const User = require('../Models/userSchema.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');
const Product = require('../Models/productSchema.js');
const Cart = require('../Models/cartSchema');

//NOTE:  Register new user
module.exports.userRegistration = (request, response) => {
  const input = request.body;

  User.findOne({ email: input.email })
    .then((result) => {
      if (result !== null) {
        return response.send('The email is already taken!.');
      } else {
        let newUser = new User({
          email: input.email,
          password: bcrypt.hashSync(input.password, 10),
        });
        newUser
          .save()
          .then((save) => {
            return response.send(newUser);
          })
          .catch((error) => {
            return response.send(error);
          });
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};

//NOTE: User Authentication
module.exports.userAuthentication = (request, response) => {
  let input = request.body;

  User.findOne({ email: input.email })
    .then((result) => {
      if (result === null) {
        return response.send(
          'Email is not yet registered. Register first before logging in'
        );
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          input.password,
          result.password
        );

        if (isPasswordCorrect) {
          return response.send({ auth: auth.createAccessToken(result) });
        } else {
          return response.send('Password is incorrect');
        }
      }
    })
    .catch((error) => {
      return response.send(error);
    });
};

///NOTE: Check out orders
module.exports.checkoutOrders = async (request, response) => {
  try {
    const userData = auth.decode(request.headers.authorization);

    const productId = request.params.productId;

    // check if the product ID is valid
    const product = await Product.findById(productId);
    if (!product) {
      return response.send({ user: null, product: 'Invalid Product ID.' });
    }

    // check if the user is authorized to purchase products
    let user = await User.findById(userData._id);
    if (!user || user.isAdmin) {
      return response.send({
        user: null,
        product: 'You are not authorized to purchase products.',
      });
    }

    // add the product to the user's createdOrders array and update the total cost
    user.createdOrder.push({ productId: productId });
    user.totalCost += product.price;

    user = await user.save();
    if (!user) {
      return response.send({
        user: null,
        product: 'There was an error during the purchase.',
      });
    }
    return response.send({ user, product });
  } catch (error) {
    return response.send({
      user: null,
      product: 'There was an error during the purchase.',
    });
  }
};

//NOTE: Retrive user details
module.exports.getProfile = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  return User.findById(userData._id).then((result) => {
    result.password = '';

    return response.send(result);
  });
};

//NOTE: Set user to admin
module.exports.setAsAdmin = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const userId = request.params.userId;

  // check if the current user is admin
  const currentUser = await User.findById(userData._id);
  if (!currentUser.isAdmin) {
    return response.send('You are not authorized to perform this action.');
  }

  // set the target user as admin
  const targetUser = await User.findById(userId);
  if (!targetUser) {
    return response.send('Invalid user ID.');
  }

  // ensure target user is not the same as the current user
  if (currentUser._id.toString() === targetUser._id.toString()) {
    return response.send('You cannot set yourself as an admin.');
  }

  targetUser.isAdmin = true;

  return targetUser
    .save()
    .then((save) => response.send(targetUser))
    .catch((error) => response.send(`Error setting user as admin: ${error}`));
};

//NOTE: Retrieve all orders for an authenticated user
module.exports.getUserOrders = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  const user = await User.findById(userData._id);
  if (!user) {
    return response.send('Invalid user ID');
  }

  let orderIds = user.createdOrder.map((order) => order.productId);
  let orders = await Product.find({ _id: { $in: orderIds } });

  return response.send(orders);
};

//NOTE: Get all orders (Admin only)
module.exports.getOrders = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  // check if user is admin
  const user = await User.findById(userData._id);
  if (!user.isAdmin) {
    return response.send('You are not authorized to view orders.');
  }

  // retrieve all orders
  const orders = await User.find({}).then((users) => {
    const allOrders = [];
    users.forEach((user) => {
      user.createdOrder.forEach((order) => {
        allOrders.push({
          userId: user._id,
          email: user.email,
          productId: order.productId,
        });
      });
    });
    return allOrders;
  });

  return response.send(orders);
};

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: { type: String, required: [true, 'Email of the course is required!'] },
  password: {
    type: String,
    required: [true, 'Password of the course is required!'],
  },
  isAdmin: { type: Boolean, default: false },
  createdOrder: [
    {
      productId: {
        type: String,
        required: true,
      },
      createdOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: 'Order Created',
      },
    },
  ],
});

module.exports = mongoose.model('User', userSchema);

const Product = require('../Models/productSchema');
const auth = require('../auth.js');

// NOTE: Adding Product Admin only
module.exports.addProduct = (request, response) => {
  let input = request.body;

  const decoded = auth.decode(request.headers.authorization);

  if (decoded === null || !decoded.isAdmin) {
    return response.send({ auth: 'Failed. Only admin can create a Product.' });
  } else {
    let newProduct = new Product({
      name: input.name,
      description: input.description,
      price: input.price,
    });

    newProduct
      .save()
      .then((product) => {
        response.send(product);
      })
      .catch((error) => {
        response.send(error);
      });

    return response.send(newProduct);
  }
};

// NOTE: Retreiving all active products
module.exports.allActiveProducts = (request, response) => {
  Product.find({ isActive: true })
    .then((result) => response.send(result))
    .catch((error) => response.send(error));
};

// NOTE :Retrieving all products
module.exports.allProducts = (request, response) => {
  Product.find({})
    .then((result) => response.send(result))
    .catch((error) => response.send(error));
};

// NOTE :Retrieve single product
module.exports.productDetails = (request, response) => {
  const productId = request.params.productId;

  Product.findById(productId)
    .then((result) => response.send(result))
    .catch((error) => response.send(erorr));
};

// NOTE: Updating product
module.exports.updateProduct = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  const productId = request.params.productId;

  const input = request.body;

  if (!userData.isAdmin) {
    return response.send("You don't have acces in this page!");
  } else {
    await Product.findOne({ _id: productId }).then((result) => {
      if (result === null) {
        return response.send('productId is invalid, please try again!');
      } else {
        let updatedProduct = {
          name: input.name,
          description: input.description,
          price: input.price,
        };

        Product.findByIdAndUpdate(productId, updatedProduct, { new: true })
          .then((result) => {
            return response.send(result);
          })
          .catch((error) => response.send(error));
      }
    });
  }
};

// NOTE: Archive Prodcut
module.exports.archiveProduct = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;

  if (!userData.isAdmin) {
    return response.send("You don't have access to this page!");
  } else {
    await Product.findOne({ _id: productId }).then((result) => {
      if (result === null) {
        return response.send('productId is invalid, please try again!');
      } else {
        Product.findByIdAndUpdate(productId, { isActive: false }, { new: true })
          .then((result) => {
            return response.send(result);
          })
          .catch((error) => response.send(error));
      }
    });
  }
};
